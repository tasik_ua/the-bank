<?php

namespace App\Console\Commands;

use App\Transaction;
use Illuminate\Console\Command;
use Illuminate\Database\Connection;

class CalcYesterdayAmount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calc:total:amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'calculate total sum of amount for yesterday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new \DateTime();
        $date->add(\DateInterval::createFromDateString('yesterday'));
        $date->format('Y-m-d');

        $transactionCollection = Transaction::whereBetween('updated_at', [$date->format('Y-m-d') .' 00:00:00', $date->format('Y-m-d') .' 23:59:59'])->get();
        $totalAmount = 0;

        foreach ($transactionCollection as $item) {

            $totalAmount += $item->amount;
        }

        echo 'Total amount: ' . $totalAmount;
    }
}
