<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = ['amount', 'user_id', 'limit', 'offset'];

    protected $hidden = [];

    protected $dates = [
        'created_at',
        'update_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
