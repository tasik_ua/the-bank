<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    const TYPE_INFO = 'info';
    const TYPE_ERROR = 'error';


    protected $table = 'logs';

    protected $fillable = ['type', 'data'];

    protected $hidden = ['date'];

    public $timestamps = false;

    static public function logger($data, $type = self::TYPE_INFO)
    {
        Log::create(['data' => $data, 'type' => $type]);
    }

}
