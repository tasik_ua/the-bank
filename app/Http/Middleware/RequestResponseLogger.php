<?php

namespace App\Http\Middleware;

use App\Log;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RequestResponseLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate(Request $request, Response $response)
    {
        $data = json_encode([
            'request' => $request->headers->all(),
            'response' => [
                'headers' => $response->headers->all(),
                'content' => $response->getContent(),
            ],
        ]);

        Log::logger($data, Log::TYPE_INFO);
    }
}
