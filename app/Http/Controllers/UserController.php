<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function store (Request $request)
    {
        $data  = $request->json()->all();
        $message = [
            'status' => 'fail',
            'message' => '',
            'data' => null
        ];

        $name = $data['name'];
        $cnp = (bool) $data['cnp'];

        if (empty($name)) {
            $message['message'] = 'Name is required!';
            return $message;
        }

        $user  = User::where('name', $name)->get()->first();

        if (empty($user)) {
            $password = str_random(10);
            $apiToken = str_random(60);
            DB::beginTransaction();

            try{
                $newUser = User::create([
                    'name' => $name,
                    'password' => bcrypt($password),
                    'cnp' => $cnp,
                    'api_token' => $apiToken,
                ]);

                if (empty($newUser)) {
                    throw new Exception('Customer not create!');
                }

                DB::commit();

                $customerData = [
                    'customerid' => $newUser->id,
                    'api_token' => $apiToken,
                    'password' => $password,
                ];

                $message['status'] = 'success';
                $message['message'] = 'Customer created!';
                $message['data'] = $customerData;


            } catch(Exception $e){
                $message['message'] = 'Customer not create!';
            }

            return $message;
        }

        $message['message'] = 'Customer with this name already registered!';
        return $message;
    }
}
