<?php

namespace App\Http\Controllers;

use App\User;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $customerid, $transactionid)
    {
        $message = [
            'status' => 'fail',
            'message' => '',
            'data' => null
        ];

        if (Auth::guard('api')->id() != $customerid) {
            $message['message'] = 'You can not view transaction! You can view only own transaction!';
            return $message;
        }

        $transaction = User::find(Auth::guard('api')->id())->transactions()->find((int) $transactionid);

        if (empty($transaction)) {
            $message['message'] = 'Transaction not found!';
            return $message;
        }

        $cacheKey = 'customerid_' . $customerid . '_' . 'transactionid_' . $transactionid;

        if (Cache::has($cacheKey)){
            $data = Cache::get($cacheKey);
        } else {
            $data = [
                'transactionid' => $transaction->id,
                'amount' => $transaction->amount,
                'date' => $transaction->updated_at->format('Y-m-d H:i:s')
            ];

            Cache::put($cacheKey, $data, 3600);
        }

        $message['status'] = 'success';
        $message['message'] = 'Transaction found!';
        $message['data'] = $data;

        return $message;
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->json()->all();
        $message = [
            'status' => 'fail',
            'message' => '',
            'data' => null
        ];

        $authId = Auth::guard('api')->id();

        if (!isset($data['customerid'])) {
            $message['message'] = 'Customerid  not set!';
            return $message;
        }

        if ($authId != $data['customerid']) {
            $message['message'] = 'You can not create a transaction for someone else\'s account .';
            return $message;
        }

        if (!isset($data['amount'])) {
            $message['message'] = 'Amount not set!';
            return $message;
        }

        $user = User::findOrFail(Auth::guard('api')->id());

        DB::beginTransaction();

        try{
            $transaction = new Transaction();
            $transaction->amount = (double) $data['amount'];
            $transaction->limit = 0;
            $transaction->offset = 0;

            $transaction = $user->transactions()->save($transaction);

            if (!$transaction) {
                throw new \Exception('Transaction not create!');
            }

            DB::commit();

            $message['status'] = 'success';
            $message['message'] = 'Transaction created!';
            $message['data'] = [
                'customerid' => $authId,
                'transactonid' => $transaction->id,
                'amount' => $transaction->amount,
                'date' => $transaction->created_at->format('Y-m-d H:i:s'),
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $message['message'] = 'Transaction not create!';
            return $message;
        }

        return $message;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'status' => 'fail',
            'message' => '',
            'data' => null,
        ];

        $data = $request->json()->all();

        if (!isset($data['amount'])) {
            $message['message'] = 'Amount not set!';
            return $message;
        }

        $amount = (double) $data['amount'];

        $transaction = User::find(Auth::guard('api')->id())->transactions()->find((int)$id);

        if (empty($transaction)) {
            $message['message'] = 'Transaction not found!';
            return $message;
        }

        DB::beginTransaction();

        try {
            $transaction->amount = $amount;

            if (!$transaction->save()) {
                throw new \Exception('Transaction not updated!');
            }

            DB::commit();

            $message['status'] = 'success';
            $message['message'] = 'Transaction updated!';
            $message['data'] = [
                'customerid' => $transaction->user_id,
                'transactionid' => $transaction->id,
                'amount' => $transaction->amount,
                'date' => $transaction->updated_at->format('Y-m-d H:i:s'),
            ];

        } catch (\Exception $e) {
            DB:rollback();
            $message['message'] = 'Transaction not updated!';
        }

        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = [
            'status' => 'fail',
            'message' => '',
        ];

        $transaction = User::find(Auth::guard('api')->id())->transactions()->find((int) $id);

        if (!empty($transaction)) {
            DB::beginTransaction();

            try{
                if (!$transaction->delete()){
                    throw new \Exception('Transaction not delete!');
                }

                DB::commit();

                $message['status'] = 'success';
                $message['message'] = 'Transaction was deleted!';

            } catch (Exception $e){
                DB::rollback();
                $message['message'] = 'Transaction not delete!';
            }

        } else {
           $message['message'] = 'Transaction not found!';
        }

        return $message;
    }


    public function filter(Request $request, Transaction $transaction)
    {
        $transactionQuery = $transaction->newQuery();
        $filterData = $request->json()->all();
        $filterDataKey = $filterData;
        $cacheKey = 'filter_';
        $message = [
            'status' => 'fail',
            'message' => '',
        ];

        if (!empty($filterData)) {
            array_walk($filterDataKey, create_function('&$i,$k','$i="$k=$i";'));
            $cacheKey .= implode($filterDataKey,"");
        }

        if (isset($filterData['customerid'])) {
            $transactionQuery->leftjoin("users", "users.id", "=", "transactions.user_id");
            $transactionQuery->where('transactions.user_id', '=', $filterData['customerid']);
        }

        if (isset($filterData['amount'])) {
            $transactionQuery->where('amount', (double) $filterData['amount']);
        }

        if (isset($filterData['date'])) {
            $transactionQuery->whereBetween('updated_at', [$filterData['date'] .' 00:00:00', $filterData['date'] .' 23:59:59']);
        }

        if (isset($filterData['offset'])) {
            $transactionQuery->offset($filterData['offset']);
        }

        if (isset($filterData['limit'])) {
            $transactionQuery->limit($filterData['limit']);
        }

        if (Cache::has($cacheKey)){
            $transactionCollection = Cache::get($cacheKey);
        } else {
            $transactionCollection = $transactionQuery->get();
            Cache::put($cacheKey, $transactionCollection, 3600);
        }

        $transactionsArray = [];

        foreach ($transactionCollection as $item) {
            $customer = User::find($item->user_id);
            $transactionsArray[] = [
                'customerName' => $customer->name,
                'customerid' => $item->user_id,
                'amount' => (float)$item->amount,
                'offset' => (float)$item->offset,
                'limit' => (float)$item->limit,
                'date' => $item->updated_at->format('Y-m-d H:i:s'),
            ];
        }

        $data = $transactionsArray;



        $message['status'] = 'success';
        $message['message'] = 'List of transacrions!';
        $message['data'] = $data;

        return $message;
    }

    public function count(Request $request, Transaction $transaction)
    {
        $message = [
            'status' => 'success',
            'message' => '',
            'data' => null,
        ];

        $userId = Auth::guard('api')->id();

        $cacheKey = 'count_transaction';

        if (Cache::has($cacheKey)){
            $data = Cache::get($cacheKey);
        } else {
            $data = $transaction::count();
            Cache::put($cacheKey, $data, 3600);
        }

        $message['data'] = $data;

        return $message;
    }

    public function countFilter(Request $request, Transaction $transaction)
    {
        $transactionQuery = $transaction->newQuery();
        $filterData = $request->json()->all();
        $filterDataKey = $filterData;
        $cacheKey = 'filter_count';
        $message = [
            'status' => 'fail',
            'message' => '',
        ];

        if (!empty($filterData)) {
            array_walk($filterDataKey, create_function('&$i,$k','$i="$k=$i";'));
            $cacheKey .= implode($filterDataKey,"");
        }

        if (isset($filterData['customerid'])) {
            $transactionQuery->leftjoin("users", "users.id", "=", "transactions.user_id");
            $transactionQuery->where('transactions.user_id', '=', $filterData['customerid']);
        }

        if (isset($filterData['amount'])) {
            $transactionQuery->where('amount', (double) $filterData['amount']);
        }


        if (isset($filterData['date'])) {
            $transactionQuery->whereBetween('updated_at', [$filterData['date'] .' 00:00:00', $filterData['date'] .' 23:59:59']);
        }


        if (Cache::has($cacheKey)){
            $count = Cache::get($cacheKey);
        } else {
            $count = $transactionQuery->count();
            Cache::put($cacheKey, $count, 3600);
        }

        $message['status'] = 'success';
        $message['message'] = 'List of transacrions!';
        $message['data'] = $count;

        return $message;
    }
}
