@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-10">
                            <form  role="form" action="" method="post" id="filterform">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <select class="form-control input-sm" name="customer" id="customer">
                                            <option value="0">--</option>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2">
                                        <input type="text" name="amount" class="form-control input-sm" id="amount" placeholder="Amount" value="">
                                    </div>
                                </div>

                              <div class="form-group">
                                    <div class="col-md-3">
                                        <input type="text" name="date" value="" class="form-control input-sm"  readonly id="datepicker" placeholder="Date">
                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-1">
                                        <button type="submit" class="btn input-sm btn-success">Filter</button>
                                    </div>

                                </div>

                            </form>
                        </div>

                        <div class="col-lg-2">
                            <form method="post" action="" id="resetFilter">
                                <div class="form-group">
                                    <div class="col-md-1">
                                        <button type="submit" class="btn input-sm btn-danger">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>CUSTOMER</th>
                            <th>AMOUNT</th>
                            <th>DATE</th>
                            <th>OFFSET</th>
                            <th>LIMIT</th>
                        </tr>
                        </thead>
                        <tbody id="datatable">

                        </tbody>
                    </table>
                    <div></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <form class="form-horizontal" role="form" action="" method="">
                                    <div class="form-group">
                                        <select class="form-control input-sm" name="pages" id="perpage">
                                            <option value="5">5</option>
                                            <option value="10" selected>10</option>
                                            <option value="50">50</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="pagination">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
    <script type="text/javascript">
        // my custom script
        window.customersList = [];
        window.perPage = 10;

        getCountOfTransaction();

        getCusmomers(JSON.stringify({'limit': window.perPage}));

        function getCusmomers(data) {
            $.ajax({
                url: 'api/transaction/filter?api_token=' + window.userToken.userToken,
                method: 'POST',
                data: data,
                dataType: 'json'
            }).done(function (data) {
                $('#datatable').empty();
                if (data.status == 'success' && data.data.length > 0) {

                    data.data.forEach(function (item) {
                        $('#datatable').append('<tr><td>' + item.customerName + '</td><td>' + item.amount + '</td><td>' + item.date + '</td><td>' + item.offset + '</td><td>' + item.limit + '</td><td><tr>');
                    });
                } else {
                    $('#datatable').append('<tr><td colspan="5"><p class="text-center">Transactions not found!</p></td><tr>');
                }

            }).fail(function (data) {
                $('#datatable').append('<tr><td colspan="5"><p class="text-center">Transactions not found!</p></td><tr>');
            });
        }

        function getCountOfTransaction() {
            $.ajax({
                url: 'api/transaction/count?api_token=' + window.userToken.userToken,
                method: 'GET',
                dataType: 'json'
            }).done(function (data) {
                if (data.status == 'success') {
                    window.countTransaction = data.data;
                    renderPagination();
                } else {
                    window.countTransaction = 0;
                }

            }).fail(function (data) {
                window.countTransaction = 0;
            });
        }

        function getCountFilterOfTransaction(data) {
            $.ajax({
                url: 'api/transaction/countFilter?api_token=' + window.userToken.userToken,
                method: 'POST',
                data: data,
                dataType: 'json'
            }).done(function (data) {
                if (data.status == 'success') {
                    window.countTransaction = data.data;
                    renderPagination();
                } else {
                    window.countTransaction = 0;
                }

            }).fail(function (data) {
                window.countTransaction = 0;
            });
        }

        function getFilterData() {
            var filterData = {};
            var customerFilter = $('#customer').val();
            var amountFilter = $('#amount').val();
            var datefilter = $("#datepicker").val();


            if (customerFilter != 0) {
                filterData['customerid'] = customerFilter;
            }

            if (amountFilter != '') {
                filterData['amount'] = amountFilter;
            }

            if (datefilter != '') {
                filterData['date'] = datefilter;
            }

            filterData['limit'] = window.perPage;

            return filterData;
        }

        function renderPagination() {
            var links = Math.ceil(window.countTransaction / parseInt(window.perPage));
            $('.pagination').empty();
            for(var i = 0; i < links; i++) {
                if (i == 0) {
                    $('.pagination').append("<li class='active'><a href='#'>" + (i+1) + "</a></li>");
                } else {
                    $('.pagination').append("<li><a href='#'>" + (i+1) + "</a></li>");
                }


            }
        }


        $('#filterform').on('submit', function (e) {
            e.preventDefault();

            var dataFilter = getFilterData();

            delete dataFilter['limit'];

            getCountFilterOfTransaction(JSON.stringify(dataFilter));
            getCusmomers(JSON.stringify(getFilterData()));
        });


        $('#resetFilter').on('submit', function (e) {
            e.preventDefault();
            getCountOfTransaction();
            getCusmomers(JSON.stringify({'limit': window.perPage}));
            renderPagination();
            $('#customer').val('0');
            $('#amount').val('');
            $("#datepicker").val('');
        })


        $("#perpage").on('change', function (e) {
            e.preventDefault();
            window.perPage = $(this).val();
            renderPagination();
            getCusmomers(JSON.stringify(getFilterData()));
        })

        $('.pagination').on('click', 'a', function (e) {
            e.preventDefault();
            var offset = (parseInt($(this).text()) - 1) * window.perPage + 1;
            var filterData = getFilterData();
            filterData['offset'] = offset;


            $('.pagination').find('li').each((function (i, e) {
                $(e).removeClass('active');
            }));

            $(this).parent().addClass('active');
            getCusmomers(JSON.stringify(filterData));
        })

    </script>
@endsection
