<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class CreateTransactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        foreach (range(1,10) as $id) {
            for($i=0; $i<30; $i++) {
                DB::table('transactions')->insert([
                    'user_id' => $id,
                    'amount' => (float) rand(100, 100000),
                    'offset' => (float) rand(10, 1000),
                    'limit' =>  rand(10, 1000000),
                    'created_at' => $faker->dateTime(),
                    'updated_at' => $faker->dateTime(),
                ]);
            }
        }
    }
}
