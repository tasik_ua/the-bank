<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class CreateUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'api_token' => str_random(60),
                'password' => bcrypt('secret'),
            ]);
        }

    }
}
