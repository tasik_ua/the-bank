<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('transaction/{customerid}/{transactionid}', 'TransactionController@index');
    Route::post('transaction', 'TransactionController@store');
    Route::put('transaction/{id}', 'TransactionController@update');
    Route::delete('transaction/{id}', 'TransactionController@destroy');
    Route::post('transaction/filter', 'TransactionController@filter');
    Route::get('transaction/count', 'TransactionController@count');
    Route::post('transaction/countFilter', 'TransactionController@countFilter');
});

Route::group(['middleware' => 'api'], function () {
    Route::post('customer/create', 'UserController@store');
});
