<?php

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@home');
});

Route::get('/', 'HomeController@index');

Auth::routes();